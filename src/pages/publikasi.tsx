import * as React from 'react';
import { NextPage, InferGetStaticPropsType } from 'next';
import styled from '@emotion/styled';

import { SectionHeader } from 'modules/posts-index';
import { ArticlesListGrid } from 'modules/home';
import { PublicationCard } from 'modules/publication';

import { PageWrapper, Content, Column } from 'components/layout';
import { Box, Heading, Stack } from 'components/design-system';

import publikasiJson from 'content/publikasi.json';
import { PublicationMap, PublicationItem } from 'types/api';

const ContentAsSection = Content.withComponent('section');

const Section = styled(ContentAsSection)`
  padding-bottom: 48px;
`;

export async function getStaticProps() {
  const publicationMap: PublicationMap[] = publikasiJson;

  return { props: { publicationMap } };
}

type PublicationPageProps = InferGetStaticPropsType<typeof getStaticProps>;

const PublicationPage: NextPage<PublicationPageProps> = ({ publicationMap }) => {
  const isValidPublication = (p: PublicationItem) => p.subject && (p.url || p.publication);
  return (
    <PageWrapper title={`Publikasi | KawalCOVID19`} description={'Publikasi'} pageTitle="Publikasi">
      <SectionHeader title={'Publikasi'} />
      <Section>
        <Column>
          <Stack spacing="lg">
            {publicationMap.map(
              ({ subtitle, children }) =>
                children.filter(isValidPublication).length > 0 && (
                  <Box key={subtitle}>
                    <Heading pb="md">{subtitle}</Heading>
                    <ArticlesListGrid>
                      {children.map(
                        publication =>
                          isValidPublication(publication) && (
                            <PublicationCard key={publication.toString()} item={publication} />
                          )
                      )}
                    </ArticlesListGrid>
                  </Box>
                )
            )}
          </Stack>
        </Column>
      </Section>
    </PageWrapper>
  );
};

export default PublicationPage;
