import * as React from 'react';
import styled from '@emotion/styled';

import { Box, Text, themeProps, TextProps } from 'components/design-system';

const NavLinkRoot = Text.withComponent('a');

interface NavLinkProps {
  title: string;
  onClick: (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => void;
  isActive?: boolean;
  scale?: TextProps['variant'];
}

const SecondaryNavigationBase = styled(NavLinkRoot)<Pick<NavLinkProps, 'isActive'>>`
  display: flex;
  align-items: center;
  margin-right: ${themeProps.space.lg}px;
  height: 36px;
  padding-top: ${themeProps.space.xxs}px;
  text-decoration: none;
  font-weight: 600;
  border-bottom-width: 2px;
  border-bottom-style: solid;
  border-bottom-color: ${props =>
    props.isActive ? `${themeProps.colors.brandred}` : 'transparent'};
  white-space: nowrap;
  cursor: pointer;

  &:hover,
  &:focus {
    border-bottom-color: ${themeProps.colors.brandred};
  }

  &:last-of-type {
    margin-right: 0;
  }
`;

const SecondaryNavWrapper = styled(Box)`
  overflow-x: auto;
  overflow-y: hidden;
  white-space: nowrap;
`;

const SecondaryNavLink: React.FC<NavLinkProps> = ({ title, isActive, scale, onClick }) => {
  return (
    <SecondaryNavigationBase variant={scale || 300} isActive={isActive} onClick={onClick}>
      {title}
    </SecondaryNavigationBase>
  );
};

SecondaryNavLink.defaultProps = {
  isActive: false,
};

export { SecondaryNavWrapper, SecondaryNavLink };
