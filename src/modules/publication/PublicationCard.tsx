import * as React from 'react';
import { SpaceProps } from 'styled-system';

import { Box, Text, Heading, Paragraph, UnstyledAnchor } from 'components/design-system';
import { OpenLinkIcon } from 'components/icons';
import { PublicationItem } from 'types/api';
import formatTime from 'utils/formatTime';

export interface PublicationCardProps extends SpaceProps {
  item: PublicationItem;
}

const PublicationCard: React.FC<PublicationCardProps> = ({ item, ...rest }) => {
  const renderPublicationItem = (spreadRest?: boolean) => {
    return (
      <Box
        display="flex"
        flexDirection="column"
        flex="1"
        backgroundColor="card"
        p="md"
        borderRadius={8}
        overflow="hidden"
        {...(spreadRest ? rest : {})}
      >
        <Box display="flex" alignItems="flex-start" justifyContent="space-between">
          <Text display="inline-block" variant={200}>
            {formatTime(new Date(item.date))}
            {item.publication && <> &middot; {item.publication}</>}
          </Text>
          {item.url && (
            <Box display="flex" ml="xs">
              <OpenLinkIcon />
            </Box>
          )}
        </Box>
        {item.subject && (
          <Heading pt="xs" variant={600}>
            {item.subject}
          </Heading>
        )}
        {item.description && (
          <Box flex="1" pt="sm">
            <Paragraph>{item.description}</Paragraph>
          </Box>
        )}
        {item.speaker && (
          <Box pt="md">
            <Text display="inline-block" variant={300}>
              Narasumber: {item.speaker}
            </Text>
          </Box>
        )}
      </Box>
    );
  };

  if (item.url) {
    return (
      <UnstyledAnchor
        href={item.url}
        target="_blank"
        rel="noopener noreferrer"
        display="flex"
        {...rest}
      >
        {renderPublicationItem()}
      </UnstyledAnchor>
    );
  }

  return renderPublicationItem(true);
};

export default PublicationCard;
